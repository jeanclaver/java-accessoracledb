package accessoracledb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author JcMoutoh
 */
public  abstract class AccessData {
    
    protected String dbIP = "192.168.33.226" , dbPort = "1521" , dbName = "dbsrver" , dbPass = "app_user" , dbUser = "app_user";
//    protected String dbIP = "10.4.0.248" , dbPort = "1521" , dbName = "xe" , dbPass = "app_user" , dbUser = "app_user";
  
    protected Connection getConnection() throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        String serverIP = this.dbIP;
        String portNumber = this.dbPort;
        String sid = this.dbName;
        String url = "jdbc:oracle:thin:@" + serverIP + ":" + portNumber + "/" + sid;
        String username = this.dbUser;
        String password = this.dbPass;
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }
    
    
}
