/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessoracledb;

import java.io.Serializable;

/**
 *
 * @author JcMoutoh
 */
public class CVMBundle implements Serializable {
   
    private String cvSegmentID;
    private String msisdn;
    private String duration;
    private String bundlesName;
    private int price;
    private String refillId;
    private String volumeRefill;
    private String bundleActive;
    private int bundleOrder;
    private String confirmationTxt;

    /**
     * @return the cvSegmentID
     */
    public String getCvSegmentID() {
        return cvSegmentID;
    }

    /**
     * @param cvSegmentID the cvSegmentID to set
     */
    public void setCvSegmentID(String cvSegmentID) {
        this.cvSegmentID = cvSegmentID;
    }

    /**
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return the bundlesName
     */
    public String getBundlesName() {
        return bundlesName;
    }

    /**
     * @param bundlesName the bundlesName to set
     */
    public void setBundlesName(String bundlesName) {
        this.bundlesName = bundlesName;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the refillId
     */
    public String getRefillId() {
        return refillId;
    }

    /**
     * @param refillId the refillId to set
     */
    public void setRefillId(String refillId) {
        this.refillId = refillId;
    }

    /**
     * @return the volumeRefill
     */
    public String getVolumeRefill() {
        return volumeRefill;
    }

    /**
     * @param volumeRefill the volumeRefill to set
     */
    public void setVolumeRefill(String volumeRefill) {
        this.volumeRefill = volumeRefill;
    }

    /**
     * @return the bundleActive
     */
    public String getBundleActive() {
        return bundleActive;
    }

    /**
     * @param bundleActive the bundleActive to set
     */
    public void setBundleActive(String bundleActive) {
        this.bundleActive = bundleActive;
    }

    /**
     * @return the bundleOrder
     */
    public int getBundleOrder() {
        return bundleOrder;
    }

    /**
     * @param bundleOrder the bundleOrder to set
     */
    public void setBundleOrder(int bundleOrder) {
        this.bundleOrder = bundleOrder;
    }

    /**
     * @return the confirmationTxt
     */
    public String getConfirmationTxt() {
        return confirmationTxt;
    }

    /**
     * @param confirmationTxt the confirmationTxt to set
     */
    public void setConfirmationTxt(String confirmationTxt) {
        this.confirmationTxt = confirmationTxt;
    }

}
