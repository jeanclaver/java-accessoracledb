/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessoracledb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import mtnz.util.logging.MLogger;

/**
 *
 * @author JcMoutoh
 */
public class DBSrverClient extends AccessData {
    
     public int insertEntry (int msisdn, String transactionId,String entryType, int price,double balanceBefore,double balanceAfter,String cellId,String region ) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("TBL_DAVIDO_ENTRY ( ");
            sb.append("DATE_CREATED, MSISDN, TRANSACTION_ID, ENTRY_TYPE, BALANCE_BEFORE, PRICE, BALANCE_AFTER, CELLID, REGION) ");
            sb.append("VALUES (sysdate,  ?, ?, ?,  ?,  ?, ?, ?,   ?) ");
            
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, msisdn);
            pstmt.setString(2, transactionId);
            pstmt.setString(3, entryType);
            pstmt.setDouble(4, balanceBefore);
            pstmt.setInt (5, price);
            pstmt.setDouble(6, balanceAfter);
            
            if(cellId != null){
                pstmt.setString(7, cellId);
            }else{
                pstmt.setNull(7, java.sql.Types.VARCHAR);
            }
            
            if(region != null){
                pstmt.setString(8, region);
            }else{
                pstmt.setNull(8, java.sql.Types.VARCHAR);
            }                       
            // execute insert SQL stetement
            pstmt .executeUpdate();

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
        // Insert DataBundles Transactions
     public int insertTransactionDetails (String msisdn, String transactionId, String packageType,String packageName,int price,String keyword, String result, String mdpResponse) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("DATABUNDLE_REVAMP_TRANSACT ( ");
            sb.append("TRANSACTION_ID,CUSTOMER_MSISDN , PACKAGE_TYPE,PACKAGE_NAME, PRICE, keyword, result, mdp_response) ");
            sb.append("VALUES (    ?,       ?,      ?,         ?,          ?,    ?,  ?,        ?) ");
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1,transactionId);
            pstmt.setString(2, msisdn);
           
            pstmt.setString(3, packageType);
            pstmt.setString(4, packageName);
            pstmt.setInt (5, price);     
           
            pstmt.setString(6, keyword);
            
            pstmt.setString(7,result);
            
            pstmt.setString(8,mdpResponse);
            
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    } 
    
   // Get All PackageTypes 
   public List<String> getPackageTypes() {
        List<String> list = new ArrayList<String>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
          
            sb.append("select packagetype_name from DATABUNDLE_PACKAGETYPE_V4 ");
            sb.append("where packagetype_active = 1 ");
            sb.append("order by packagetype_sequence");       
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                String packageType = rs.getString("packagetype_name");
                
                list.add(packageType);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
      
 // Get Default  PackageTypes
   public List<String> getDefaultPackageTypes() {
        List<String> list = new ArrayList<String>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
          
            sb.append("select packagetype_name from DATABUNDLE_PACKAGETYPE_V4 ");
            sb.append("where packagetype_active = 1 ");
            sb.append("and  packagetype_id  != 1");
            sb.append("order by packagetype_sequence");       
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                String packageType = rs.getString("packagetype_name");
                
                list.add(packageType);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
   
    // Get Default Bundles
    public List<DataPackage> getDefaultPackages(String packageType) {
        List<DataPackage> list = new ArrayList<DataPackage>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("select * from DATABUNDLE_PACKAGE_LIVE pkg, DATABUNDLE_PACKAGETYPE_V4 pkgt "); 
            sb.append("where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
            sb.append("and  packagetype_id  != 1");
            sb.append("and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1 ");
            sb.append("order by pkg.package_order ");

            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                DataPackage db = new DataPackage();
                db.setPackageType(packageType);
                String packageName = rs.getString("package_name");
                db.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                db.setKeyword(keyword);
                String addonkeyword = rs.getString("addon_keyword");
                db.setAddonKeyword(addonkeyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                db.setKeywordMoMo(keywordMoMo);
                 String addonkeywordmomo = rs.getString("addon_keyword_momo");
                db.setAddonKeywordMomo(addonkeywordmomo);
                int price = rs.getInt("package_price");
                db.setPrice(price); 
                int addonprice = rs.getInt("addon_package_price");
                db.setAddonPrice(addonprice);
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                db.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                db.setCreditTxt(creditTxt);
                
                 String addonheader = rs.getString("addon_header_Text");
                db.setAddonheader(addonheader);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                db.setConfirmationTxt(confirmationTxt);
                
                String addonconfirmationTxt = rs.getString("addon_package_confirmation_Txt");
                db.setAddonconfirmationTxt(addonconfirmationTxt);
                 String Duration = rs.getString("DURATION");
                db.setDuration(Duration);
               
                
               
                list.add(db);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    // Get Addon Packages
    
     public DataPackage getAddonPackages(String packageType , String packageName  , int packageOrder) {
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        DataPackage dataPackage = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
          sb.append("select * from DATABUNDLE_PACKAGE_LIVE pkg, DATABUNDLE_PACKAGETYPE_V4 pkgt "); 
          sb.append(" where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
          sb.append(" and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1  and PACKAGE_NAME='"+packageName+"'" );
          sb.append(" and pkg.addon_header_text is not null");
          sb.append(" and pkg.package_order = '"+packageOrder+"'");
//          sb.append(" Select * from DATABUNDLE_PACKAGE_V5 where PACKAGE_TYPE= ="+packageType+" and PACKAGE_ORDER="+packageOrder); 
          
          
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                DataPackage dc = new DataPackage();
                dc.setPackageType(packageType);
                //String packageName = rs.getString("package_name");
                dc.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                dc.setKeyword(keyword);
                String addonkeyword = rs.getString("addon_keyword");
                dc.setAddonKeyword(addonkeyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                dc.setKeywordMoMo(keywordMoMo);
                 String addonkeywordmomo = rs.getString("addon_keyword_momo");
                dc.setAddonKeywordMomo(addonkeywordmomo);
                int price = rs.getInt("package_price");
                dc.setPrice(price); 
                int addonprice = rs.getInt("addon_package_price");
                dc.setAddonPrice(addonprice);
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                dc.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                dc.setCreditTxt(creditTxt);
                
                 String addonheader = rs.getString("addon_header_Text");
                dc.setAddonheader(addonheader);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                dc.setConfirmationTxt(confirmationTxt);
                
                String addonconfirmationTxt = rs.getString("addon_package_confirmation_Txt");
                dc.setAddonconfirmationTxt(addonconfirmationTxt);
                String Duration = rs.getString("DURATION");
                dc.setDuration(Duration);
                
               dataPackage =dc;
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return dataPackage;
    }
    
     
      //Get the GoodYear Msisdns
     
     public GoodYearMsisdn getEligibleMsisdn(String msisdn) {
        GoodYearMsisdn resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from APP_USER.GOOD_YEAR_MSISDN where MSISDN = "+msisdn);
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                String num = rs.getString("MSISDN");

                resp = new GoodYearMsisdn();
                resp.setMsisdn(num);           
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
      // Get all  Bundles
    public List<DataPackage> getPackages(String packageType) {
        List<DataPackage> list = new ArrayList<DataPackage>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("select * from DATABUNDLE_PACKAGE_LIVE pkg, DATABUNDLE_PACKAGETYPE_V4 pkgt "); 
            sb.append("where pkg.PACKAGE_TYPE = pkgt.packagetype_id ");
            sb.append("and pkgt.packagetype_name ='"+packageType+"' and pkg.package_active = 1 ");
            sb.append("order by pkg.package_order ");

            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                DataPackage dc = new DataPackage();
                dc.setPackageType(packageType);
                String packageName = rs.getString("package_name");
                dc.setPackageName(packageName);
                String keyword = rs.getString("package_keyword");
                dc.setKeyword(keyword);
                String addonkeyword = rs.getString("addon_keyword");
                dc.setAddonKeyword(addonkeyword);
                String keywordMoMo = rs.getString("package_keyword_MoMo");
                dc.setKeywordMoMo(keywordMoMo);
                 String addonkeywordmomo = rs.getString("addon_keyword_momo");
                dc.setAddonKeywordMomo(addonkeywordmomo);
                int price = rs.getInt("package_price");
                dc.setPrice(price); 
                int addonprice = rs.getInt("addon_package_price");
                dc.setAddonPrice(addonprice);
                
                String moMoTxt = rs.getString("package_MoMo_Text");
                dc.setMoMoTxt(moMoTxt);
                
                String creditTxt = rs.getString("package_Credit_Text");
                dc.setCreditTxt(creditTxt);
                
                 String addonheader = rs.getString("addon_header_Text");
                dc.setAddonheader(addonheader);
                
                String confirmationTxt = rs.getString("package_confirmation_Text");
                dc.setConfirmationTxt(confirmationTxt);
                
                String addonconfirmationTxt = rs.getString("addon_package_confirmation_Txt");
                dc.setAddonconfirmationTxt(addonconfirmationTxt);
                String Duration = rs.getString("DURATION");
                dc.setDuration(Duration);
                
               
                
               
                list.add(dc);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
    //Get eligible subscribers for CVM
     public CVMBundle getEligibleCVM(String msisdn) {
        CVMBundle resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("select * from APP_USER.ALL_CVM_MSISDN where MSISDN = "+msisdn);
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if( rs.next() )
            {

                String num = rs.getString("MSISDN");

                resp = new CVMBundle();
                resp.setMsisdn(num);           
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }
    
     //Get CVM Type
      // Get Default  PackageTypes
   public List<String> getCvmTypes() {
        List<String> list = new ArrayList<String>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
                        
          
            sb.append("select cvm_segment_id from  APP_USER.ALL_CVM_MSISDN ");  
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                String packageType = rs.getString("packagetype_name");
                
                list.add(packageType);
                
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
   
     
     
     
     
     //Get CV BUNDLES
     
           
    public List<CVMBundle> getCVMPackages(String msisdn) {
        List<CVMBundle> list = new ArrayList<CVMBundle>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();                       
                                
            sb.append("select * from  APP_USER.ALL_CVM_BUNDLES pkg, APP_USER.ALL_CVM_MSISDN pkgt "); 
            sb.append("where pkg.CVM_SEGMENT_ID = pkgt.CVM_SEGMENT_ID ");
            sb.append("and pkgt.MSISDN ='"+msisdn+"' and pkg.BUNDLE_ACTIVE = 1  order by pkg.BUNDLE_ORDER ");
           

            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                CVMBundle cv = new CVMBundle();
                cv.setMsisdn(msisdn);
                String segmentId =  rs.getString("CVM_SEGMENT_ID");
                cv.setCvSegmentID(segmentId);
                String bundlesName = rs.getString("BUNDLE_NAME");
                cv.setBundlesName(bundlesName);
                String duration = rs.getString("DURATION");
                cv.setDuration(duration);
                String refillId = rs.getString("REFILL_ID");
                cv.setRefillId(refillId);
                String volumeRefill = rs.getString("VOLUME_REFILL");
                cv.setVolumeRefill(volumeRefill);
                int price = rs.getInt("PRICE");
                cv.setPrice(price); 
                int bundleOrder = rs.getInt("BUNDLE_ORDER");
                cv.setBundleOrder(bundleOrder);
                             
                String confirmationTxt = rs.getString("BUNDLE_CONFIRMATION_TEXT");
                cv.setConfirmationTxt(confirmationTxt);
                
                String bundleActive = rs.getString("BUNDLE_ACTIVE");
                cv.setBundleActive(bundleActive);
                              
                list.add(cv);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
   
      // Insert cvBundles Transactions
     public int insertCvmTransactionDetails ( String transactionId,String msisdn,int sc, String cvSegmentID,String bundlesName,int price,String refillId, String duration, String volumeRefill, String Response) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("APP_USER.ALL_CVM_TRANSAC ( ");
            sb.append("TRANSACTION_ID,MSISDN , SERVICE_CLASS,CVM_SEGMENT_ID, BUNDLE_NAME, PRICE, REFILL_ID, DURATION, VOLUME_REFILL,  RESPONSE) ");
            sb.append("VALUES (    ?,       ?,      ?,         ?,     ?,        ?,    ?,  ?,     ?,     ?) ");
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setString(1,transactionId);
            pstmt.setString(2, msisdn);
            pstmt.setInt (3, sc);   
            pstmt.setString(4, cvSegmentID);
            pstmt.setString(5, bundlesName);
            pstmt.setInt (6, price);     
            pstmt.setString(7, refillId);
            pstmt.setString(8,duration);
            pstmt.setString(9,volumeRefill);
            pstmt.setString(10,Response);
            
            // execute insert SQL stetement
            pstmt .executeUpdate();
            
                  

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    } 
}
